export interface IProduct {
  IDproduct: Number;
  name: string;
  description: string;
  heurekaName: string;
}
