import express, { Application, Request, Response, NextFunction } from 'express';

import cors from 'cors';
import bodyparser from 'body-parser';

import ProductRouter from './api/ProductRouter';

let app: Application = express();

const PORT: Number = 8080;

app.use('/api/product', cors(), bodyparser.json(), ProductRouter);
app.use(bodyparser.json());

//Error handler midlleware
app.use((err: any, req: Request, res: Response, next: NextFunction) => {
  res.status(409).send({ error: err.message });
});

app.get('/', (req: Request, res: Response, next: NextFunction) => {
  res.send({ message: 'sechno jede jak ma' });
});

// error handler middleware

app.listen(PORT, () => console.log(`Your port is ${PORT}`));
