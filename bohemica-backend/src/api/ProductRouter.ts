import express, { Router } from 'express';

import * as ProductController from '../controller/ProductController';

let productRoutes: Router = express.Router();

productRoutes.get('/:par_input', ProductController.GetProducts);

export default productRoutes;
