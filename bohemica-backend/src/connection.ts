import { serverConfig } from './config';
import { Connection } from 'tedious';


let connection = new Connection(serverConfig);

connection.on('connect', (err: any) => {
    if (err) console.error('error connecting :-(', err);
    else {
        console.log('successfully connected!!');
    }
});


export default connection;