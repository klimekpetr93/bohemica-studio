import connection from '../connection';
import { Request } from 'tedious';
import { IProduct } from '../model/IProduct';

class Product implements IProduct {
  IDproduct = 0;
  name = '';
  description = '';
  heurekaName = '';
}

export const GetProducts = (req: any, response: any, next: any) => {
  let responseProduct: Array<Object> = [];
  let wasError = false;
  let request = new Request(
    "SELECT IDproduct, heurekaName, description, name FROM tb_product t WHERE t.name like '%" + req.params.par_input + "%'  ",
    (err: any, rowCount: any) => {
      if (err) {
        next(err);
        wasError = true;
      }
    },
  );

  request.on('row', (columns: any) => {
    let product = new Product();
    for (let i = 0; i < Object.keys(product).length; i++) {
      let nazev: string = Object.keys(product)[i];
      product = {
        ...product,
        [nazev]: columns[Object.keys(product)[i]].value,
      };
    }
    responseProduct.push(product);
  });

  request.on('requestCompleted', function () {
    if (!wasError) response.json(responseProduct);
  });

  connection.execSql(request);
};
